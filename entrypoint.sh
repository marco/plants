#!/bin/bash

declare -r WORKDIR=${WORKDIR:-/tmp/plantsdb}
declare -r ES_HOST=${ES_HOST:-elasticsearch}
declare -r ES_PORT=${ES_PORT:-9200}


if [[ ${ENV} == 'DEV' ]]; then
    DEBUG="--debug"
fi

./app.py ${DEBUG} --host="0.0.0.0" --port=9000 --secret="${SECRET}" --workdir="${WORKDIR}" \
    --es_host=${ES_HOST} --es_port=${ES_PORT}
