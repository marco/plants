# Copyright AlertAvert.com (c) 2017. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import json
import os

import requests

import importxls


class ElasticsearchConnector(object):
    HEALTH = "_cat/health"
    HEADERS = {'Accept': 'application/json'}
    METADATA_DOCTYPE = "metadata"

    def __init__(self, index, doctype, host='localhost', port=9200):
        self._url = "http://{}:{}".format(host, port)
        self._index = index
        self._doctype = doctype

    def upload(self, data):
        """ Uploads the data to the Elasticsearch server.
        
        :param data: the plants' database
        :type data: list[dict]
        
        :return: None 
        """
        assert isinstance(data, list)

        print("Connecting to:", self._url)
        res = requests.get("{}/{}".format(self._url, ElasticsearchConnector.HEALTH),
                           headers=ElasticsearchConnector.HEADERS)

        if not res.ok:
            print("Server unavailable")
            return

        status = res.json()[0].get('status')
        print("Status:", status)
        if status == 'red':
            print("Elasticsearch server is not in a healthy state, aborting uploads")
            return

        print("Inserting {} items in the '{}' index".format(len(data), self._index))
        count = 0
        for item in data:
            if 'botanical_name' not in item:
                continue
            res = requests.post("{}/{}/{}".format(self._url, self._index, self._doctype),
                                headers=ElasticsearchConnector.HEADERS, json=item)
            if not res.ok:
                print("Failed ({}): {}".format(res.status_code, res.reason))
                continue
            count += 1
        print("SUCCESS {} records uploaded".format(count))

    def upload_metadata(self, upload_id, upload_metadata):
        res = requests.post("{}/{}/{}/{}".format(self._url, self._index,
                                                 ElasticsearchConnector.METADATA_DOCTYPE,
                                                 upload_id),
                            headers=ElasticsearchConnector.HEADERS, json=upload_metadata)
        if not res.ok:
            print("Failed ({}): {}".format(res.status_code, res.reason))
        return res.ok

    def create_index(self):
        print("Creating '{}' index".format(self._index))
        res = requests.put("{}/{}".format(self._url, self._index))
        if not res.ok:
            print("Failed to create the index, aborting")
            exit(1)

    def wipe_index(self):
        print("Deleting index {} from Elasticsearch server".format(self._index))
        requests.delete("{}/{}".format(self._url, self._index))

    def rebuild_index(self, local_path, keep=False):
        if not os.path.exists(local_path):
            print("File {} does not exist".format(local_path))
            return
        self.wipe_index()
        data, stats = importxls.import_xls(local_path)
        self.upload(data)
        print("Data uploaded from {}".format(local_path))
        if not keep:
            os.remove(local_path)
            print("Data file {} removed".format(local_path))
        if self.upload_metadata(999, stats):
            return 999

    def find_one(self, doc_id):
        return requests.get("{}/{}/{}/{}".format(self._url, self._index, self._doctype, doc_id))

    def find_metadata(self, doc_id):
        return requests.get("{}/{}/{}/{}".format(self._url, self._index,
                                                 ElasticsearchConnector.METADATA_DOCTYPE,
                                                 doc_id))

    def search_for(self, query):
        return requests.post("{}/{}/_search".format(self._url, self._index),
                             data=json.dumps(query))
