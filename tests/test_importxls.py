import unittest

from importxls import sanitize_field_names, import_xls


class ImportTests(unittest.TestCase):
    def test_sanitize(self):
        self.assertListEqual(["valid_name"], sanitize_field_names(["Valid; NAME"]))
        self.assertListEqual(["botanic_name"], sanitize_field_names(['Botanic Name']))
        self.assertListEqual(["botanic_name"], sanitize_field_names(['    Botanic; Name']))

    def test_parse(self):
        workbook = 'data/test.xlsx'
        plants, stats = import_xls(workbook)
        names = ['Trees', 'Shrubs', 'Perennials', "Ornamental grasses", "Trees_2"]
        for name in names:
            self.assertIn(name, stats['worksheets'])

        self.assertSequenceEqual([1, 1, 26, 77], stats['worksheets']['Perennials']['dimensions'])
        self.assertEqual(6, stats['worksheets']['Trees_2']['rows'])

        self.assertEqual(15, stats['worksheets']['Trees']['rows'])
        self.assertEqual(42, stats['worksheets']['Shrubs']['rows'])
        self.assertEqual(17, stats['worksheets']['Ornamental grasses']['rows'])
        self.assertEqual(76, stats['worksheets']['Perennials']['rows'])

        self.assertEqual(156, stats['total'])

        self.assertEqual('Trees_2', stats['errors'][2]['worksheet'])
        self.assertNotEqual(-1, stats['errors'][2]['error'].find('16'))
