#!/usr/bin/env  python3
#
# Copyright AlertAvert.com (c) 2017. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import argparse
from copy import deepcopy
from flask import (
    Flask,
    make_response,
    redirect,
    render_template,
    request,
    url_for,
)
import json
import os
from werkzeug.utils import secure_filename

from elasticsearch_connector import ElasticsearchConnector


DOCTYPE = 'plants'
INDEX_NAME = 'cfgreendesign'
UPLOAD_FOLDER = '/tmp'


def load_template():
    with open('templates/query.json') as template:
        return json.load(template)


TEMPLATE = load_template()
HEADERS = {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
}
ALLOWED_EXTENSIONS = {'xls', 'xlsx'}

MAPPING = [
    {"key": "botanical_name", "caption": "Botanical", "width": "10%"},
    {"key": "common_name", "caption": "Name", "width": "10%"},
    {"key": "description", "caption": "Description", "width": "20%"},
    {"key": "flowering_months", "caption": "Flowering", "width": "10%"},
    {"key": "quantity", "caption": "Qty", "width": "5%"},
    {"key": "plant_size_at_maturity", "caption": "Plant size at Maturity", "width": "10%"},
    {"key": "qualify_for_rebate", "caption": "Rbt", "width": "5%"},
    {"key": "water_needs_according_to_wucols", "caption": "Water", "width": "5%"},
    {"key": "pot_size", "caption": "Pot", "width": "5%"},
    {"key": "native", "caption": "Native", "width": "5%"},
    {"key": "light", "caption": "Light", "width": "5%"},
    {"key": "attract_butterflies", "caption": "Attract", "width": "10%"},
    {"key": "classification", "caption": "Cat", "width": "5%"},
    {"key": "attract_bees", "caption": "Bees?", "width": "5%"}
]

PLANT_KEYS = [
    {"key": "classification", "caption": "Classification"},
    {"key": "description", "caption": "Description"},
    {"key": "full_description", "caption": "Full Description"},
    {"key": "flowering_months", "caption": "Flowering Months"},
    {"key": "quantity", "caption": "Quantity"},
    {"key": "plant_size_at_maturity", "caption": "Plant size at Maturity"},
    {"key": "water_needs_according_to_wucols", "caption": "Water needs according to WUCOLS"},
    {"key": "pot_size", "caption": "Recommended Pot Size"},
    {"key": "native", "caption": "California Native?"},
    {"key": "light", "caption": "Light"},
    {"key": "attract_butterflies", "caption": "Attracts birds & hummingbirds?"},
    {"key": "attract_bees", "caption": "Attracts Bees?"},
    {"key": "qualify_for_rebate", "caption": "Qualifies for Rebate?"},
]

app = Flask(__name__)


@app.route('/')
def entry():
    filename = request.args.get('filename')
    errmsg = request.args.get('errmsg')
    upload_id = request.args.get('upload_id')
    return render_template('search.html', uploaded_file=filename, msg=errmsg, upload_id=upload_id)


@app.route('/search')
def search():
    query_args = request.args.get('q')
    offset = int(request.args.get('offset', 0))
    size = int(request.args.get('size', 25))
    query = build_query(query_args, offset, size)
    connector = app.config['ES_HOST']
    response = connector.search_for(query)
    if response.ok:
        return render_template("results.html",
                               results=process_results(response.json()),
                               meta=MAPPING)
    return make_response("Failed: {}".format(response.reason), 400)


@app.route('/plant/<id>')
def get_plant(id):
    connector = app.config['ES_HOST']
    response = connector.find_one(id)
    if response.ok:
        return render_template("plant.html", plant=response.json().get("_source"), meta=PLANT_KEYS)
    else:
        return redirect(url_for('entyr', errmsg="Could not find plant ({} id missing)".format(id)))


@app.route('/import', methods=['POST'])
def upload_file():
    # check if the post request has the file part
    file = request.files.get('importFile')
    if not file:
        app.logger.error("Missing import file name: ", request.files)
        return redirect(url_for('entry', errmsg="Missing file"))
    app.logger.info("Uploading file '%s'", file.filename)
    # if user does not select file, browser also
    # submit a empty part without filename
    filename = secure_filename(file.filename)
    if filename == '' or not allowed_file(filename):
        app.logger.error("Not a valid filename: %s", filename)
        return redirect(url_for('entry', errmsg="You must select a valid Excel file"))

    local_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)
    file.save(local_path)
    app.logger.info("Uploading data to Elasticsearch server")
    try:
        connector = app.config['ES_HOST']
        stats_id = connector.rebuild_index(local_path)
        return redirect(url_for('entry', filename=filename, upload_id=stats_id))
    except Exception as ex:
        app.logger.error("Cannot upload file: {}. Reason: {}".format(filename, ex))
        return redirect(url_for('entry', errmsg="Error while importing data from '{}' ({})".format(
            filename, ex)))


@app.route('/uploads/<upload_id>')
def show_metadata(upload_id):
    connector = app.config['ES_HOST']
    response = connector.find_metadata(upload_id)
    if response.ok:
        return render_template("metadata.html", metadata=response.json().get("_source"))
    else:
        return redirect(url_for('entry', errmsg="Could not find details for upload ({} id "
                                                "missing)".format(upload_id)))



def process_results(results):
    items = list()
    if "hits" in results:
        if "hits" in results.get("hits"):
            hits = results["hits"]["hits"]
            for hit in hits:
                item = hit.get("_source")
                item['id'] = hit["_id"]
                items.append(item)
    return items


def build_query(search_terms, offset=0, size=25):
    query = deepcopy(TEMPLATE)
    query["query"]["multi_match"]["query"] = search_terms
    query['from'] = offset
    query['size'] = size
    return query


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", action='store_true', default=False)
    parser.add_argument("--host")
    parser.add_argument("--port", type=int, default=8000)
    parser.add_argument("--es_host", required=True)
    parser.add_argument("--es_port", type=int, required=True)
    parser.add_argument("--secret", required=True)
    parser.add_argument("--workdir", default=UPLOAD_FOLDER)
    return parser.parse_args()


if __name__ == '__main__':
    cfg = parse_args()
    app.config['ES_HOST'] = ElasticsearchConnector(INDEX_NAME, DOCTYPE,
                                                   host=cfg.es_host,
                                                   port=cfg.es_port)
    app.config['UPLOAD_FOLDER'] = cfg.workdir
    app.config['SECRET'] = cfg.secret
    app.secret_key = cfg.secret

    app.run(host=cfg.host, port=cfg.port, debug=cfg.debug)
