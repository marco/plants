# Copyright AlertAvert.com (c) 2017. All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Reads in a spreadsheet and plays with it.
# See: https://automatetheboringstuff.com/chapter12/

import openpyxl

MONTHS = 12
SANITIZE_TRANSLATE = str.maketrans({
    " ": "_",
    ";": None,
    "'": None,
    '"': None,
    "?": None,
    "/": "_"
})


def make_error(worksheet, row, message):
    return {
        'worksheet': worksheet,
        'row': row,
        'error': message
    }


def import_xls(workbook):
    """ Reads an Excel workbook and converts into an object suitable to being uploaded to 
    Elasticsearch via JSON.
    
    :param workbook: the name of the XLS file containing the plants' data.
    :type workbook: str
    :return: the full plants database as a Python object
    :rtype: list[dict]
    """
    plants = openpyxl.load_workbook(filename=workbook, read_only=True)
    plant_names = plants.get_sheet_names()
    plants_db = list()

    stats = {
        'worksheets': dict(),
        'total': 0,
        'errors': list()
    }

    for name in plant_names:
        worksheet = plants.get_sheet_by_name(name)
        print("Processing: ", name)
        rows = worksheet.rows

        fields = list()
        for row in rows:
            for cell in row:
                fields.append(cell.value)
            break
        else:
            print("Empty sheet, terminating: ", name)
            # noinspection PyTypeChecker
            stats['errors'].append(make_error(name, None, "Empty sheet, terminating"))
            break

        names = sanitize_field_names(fields[:-MONTHS])
        month_names = fields[-MONTHS:]

        wstats = {
            'fields': fields,
            'dimensions': [worksheet.min_column, worksheet.min_row, worksheet.max_column,
                           worksheet.max_row],
            'rows': 0
        }

        row_nr = 1
        for row in rows:
            plant = {
                'classification': name,
                'flowering_months': list()
            }
            has_data = False
            for col_nr, cell in enumerate(row):
                if cell.value:
                    has_data = True
                    if col_nr < len(names):
                        plant[names[col_nr]] = cell.value
                    else:
                        plant['flowering_months'].append(month_names[col_nr - len(names)])
            # When we encounter an empty row, we assume the worksheet is done.
            if not has_data:
                # noinspection PyTypeChecker
                stats['errors'].append(make_error(name, row_nr,
                    "Empty row or self-detected row number ({}) is not correct".format(
                        wstats['dimensions'][3])))
                break

            # Prevent rows without Botanical Name to make it to the DB
            if 'botanical_name' in plant:
                wstats['rows'] += 1
                plants_db.append(plant)
            else:
                # noinspection PyTypeChecker
                stats['errors'].append(make_error(name, row_nr, "Missing 'Botanical Name' field"))

            row_nr += 1

        stats['worksheets'][name] = wstats
        stats['total'] += wstats['rows']

    return plants_db, stats


def sanitize_field_names(fields):
    """ Makes all elements in `fields` more "JSON-friendly" when building ES queries.
    
    Converts all to lower case, replaces spaces with `_` (underscore) and removes all quotes.
    
    :param fields: the field names to be sanitized
    :type fields: list[str]
    
    :return: a new list with all field names sanitized
    :rtype: list[str]
    """
    results = list()
    for name in fields:
        results.append(name.strip().lower().translate(SANITIZE_TRANSLATE))
    return results
