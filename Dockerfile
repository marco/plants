# Copyright AlertAvert.com (c) 2017. All rights reserved.

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#    http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM python:3.5
MAINTAINER M. Massenzio <marco@alertavert.com>

WORKDIR /opt/plantsdb

# Let's build the underlying layer first: this changes only rarely, if at all.
# This should speed up subsequent builds.
COPY requirements.txt ./
RUN pip install --upgrade pip && pip install -r requirements.txt
RUN groupadd -r flask && useradd -r -g flask flask

COPY . /opt/plantsdb

USER flask
EXPOSE 9000

CMD ["/opt/plantsdb/entrypoint.sh"]
