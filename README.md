# Plants Database

![Apache 2](https://go-shields.herokuapp.com/license-apache2-blue.png)

Project    |  PlantsDb
---        | ----
Date       | 2017-04-07
Author     | Marco Massenzio
Version    | 0.1.0
Updated    | 2017-04-08

>A simple Flask application to import and search a database of plants, originally maintained in
an Excel spreadsheet: allows importing the data from the workbook and then entering free text
searches.


## Build & Run

This is meant to be run using Docker containers:

- run Elasticsearch in its own container:
```
    docker run --name esplants elasticsearch:5
```
  (no need to expose ports, as we will link directly to it, unless you want to
  access it directly from your host, in which case you can add `-p 9200:9200`).

- build the Plants DB container (or simply use the one uploaded to the Docker Hub):
```
    docker build -t massenz/plantsdb:${VERSION} .
```
  (note the trailing dot - or Docker will be mad at you).

- then run it, linking it to the running Elasticsearch server (pick whichever spare
  port you please on your host) - note the `workdir` is a directory on the container:
```
    docker run --rm --link esplants -e ES_HOST=esplants -d \
      -e SECRET=zekret -e WORKDIR=/tmp/pts -p8000:9000 \
      massenz/plantsdb:${VERSION}
```
Currently `VERSION` is at `0.1`, but you can also omit it and `latest` will be used.

## Usage

Simply point your browser to http://localhost:8000 (or whatever port you have chosen
for the `-p` argument) and then upload the spreadsheet (there is a `demo.xlsx` in the
`tests/data/` directory) with the data to initialize the ES index; once you get the
success message, you can start executing searches.

![Search Results](images/plants-screen.png)
